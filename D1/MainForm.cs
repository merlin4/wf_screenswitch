﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D3
{
    public partial class MainForm : Form
    {
        private int _counter;
        private ScreenA _screenA;
        private ScreenB _screenB;

        public MainForm()
        {
            InitializeComponent();
            _counter = 0;
            _screenA = new ScreenA(this);
            _screenB = new ScreenB(this);
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            ShowScreenA();
        }

        public int Counter
        {
            get { return _counter; }
        }

        public void ShowScreenA()
        {
            ++_counter;
            _screenB.Hide();
            _screenA.Show();
            _screenA.Location = this.Location;
        }

        public void ShowScreenB()
        {
            ++_counter;
            _screenA.Hide();
            _screenB.Show();
            _screenB.Location = this.Location;
        }
    }


}
