﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D3
{
    public partial class ScreenA : Form
    {
        private MainForm _form;

        public ScreenA(MainForm form)
        {
            InitializeComponent();
            _form = form;
        }

        private void switchButton_Click(object sender, EventArgs e)
        {
            _form.ShowScreenB();
        }

        private void ScreenA_Shown(object sender, EventArgs e)
        {
            //label1.Text = "A" + _form.Counter;
        }

        private void ScreenA_VisibleChanged(object sender, EventArgs e)
        {
            label1.Text = "A" + _form.Counter;
        }
    }
}
