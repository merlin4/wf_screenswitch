﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D3
{
    public partial class ScreenB : Form
    {
        private MainForm _form;

        public ScreenB(MainForm form)
        {
            InitializeComponent();
            _form = form;
        }

        private void switchButton_Click(object sender, EventArgs e)
        {
            _form.ShowScreenA();
        }

        private void ScreenB_Shown(object sender, EventArgs e)
        {
            //label1.Text = "B" + _form.Counter;
        }

        private void ScreenB_VisibleChanged(object sender, EventArgs e)
        {
            label1.Text = "B" + _form.Counter;
        }
    }
}
