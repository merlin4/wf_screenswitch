﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D3
{
    public partial class MainForm : Form
    {
        private int _counter;
        private ScreenA _screenA;
        private ScreenB _screenB;

        public MainForm()
        {
            InitializeComponent();
            _counter = 0;
            _screenA = new ScreenA(this);
            _screenA.SwitchScreen += ScreenA_SwitchScreen;
            _screenB = new ScreenB(this);
            _screenB.SwitchScreen += ScreenB_SwitchScreen;
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            ShowScreenA();
        }

        private void ScreenA_SwitchScreen(object sender, EventArgs e)
        {
            ShowScreenB();
        }

        private void ScreenB_SwitchScreen(object sender, EventArgs e)
        {
            ShowScreenA();
        }

        public int Counter
        {
            get { return _counter; }
        }

        private void ShowScreenA()
        {
            ++_counter;
            _screenB.Hide();
            _screenA.Show();
            _screenA.Location = this.Location;
        }

        private void ShowScreenB()
        {
            ++_counter;
            _screenA.Hide();
            _screenB.Show();
            _screenB.Location = this.Location;
        }
    }


}
