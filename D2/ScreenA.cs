﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D3
{
    public partial class ScreenA : Form
    {
        private int _counter;

        public ScreenA(int counter)
        {
            InitializeComponent();
            _counter = counter;
            label1.Text = "A" + counter;
        }

        private void switchButton_Click(object sender, EventArgs e)
        {
            var screenB = new ScreenB(_counter + 1);
            screenB.Show();
            screenB.Location = this.Location;
            this.Close();
        }

        private void ScreenA_Shown(object sender, EventArgs e)
        {
        }

        private void ScreenA_VisibleChanged(object sender, EventArgs e)
        {
        }
    }
}
