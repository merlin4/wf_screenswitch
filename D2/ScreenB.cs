﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D3
{
    public partial class ScreenB : Form
    {
        private int _counter;

        public ScreenB(int counter)
        {
            InitializeComponent();
            _counter = counter;
            label1.Text = "A" + counter;
        }

        private void switchButton_Click(object sender, EventArgs e)
        {
            var screenA = new ScreenA(_counter + 1);
            screenA.Show();
            screenA.Location = this.Location;
            this.Close();
        }

        private void ScreenB_Shown(object sender, EventArgs e)
        {
        }

        private void ScreenB_VisibleChanged(object sender, EventArgs e)
        {
        }
    }
}
