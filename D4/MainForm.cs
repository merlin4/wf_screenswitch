﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D4
{
    public partial class MainForm : Form
    {
        private int _counter;
        private ScreenA _screenA;
        private ScreenB _screenB;

        public MainForm()
        {
            InitializeComponent();
            _counter = 0;

            _screenA = new ScreenA(this);
            _screenA.Dock = DockStyle.Fill;
            _screenA.Visible = false;
            this.Controls.Add(_screenA);

            _screenB = new ScreenB(this);
            _screenB.Dock = DockStyle.Fill;
            _screenB.Visible = false;
            this.Controls.Add(_screenB);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ShowScreenA();
        }

        public int Counter
        {
            get { return _counter; }
        }

        public void ShowScreenA()
        {
            ++_counter;
            _screenA.Visible = true;
            _screenB.Visible = false;
        }

        public void ShowScreenB()
        {
            ++_counter;
            _screenB.Visible = true;
            _screenA.Visible = false;
        }
    }
}
