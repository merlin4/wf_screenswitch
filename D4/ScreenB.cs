﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D4
{
    public partial class ScreenB : UserControl
    {
        private MainForm _form;

        public ScreenB(MainForm form)
        {
            InitializeComponent();
            _form = form;
        }

        private void switchButton_Click(object sender, EventArgs e)
        {
            _form.ShowScreenA();
        }
        
        private void ScreenB_VisibleChanged(object sender, EventArgs e)
        {
            label1.Text = "B" + _form.Counter;
        }
    }
}
